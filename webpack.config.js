var debug = false// process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path')

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : false,
  devServer: { inline: true },
  entry: "./js/game.js",
  output: {
   // path: __dirname + "/src/",
    filename: "./js/game.min.js"
  },
  module: {
  loaders: [
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel-loader',
      query: {
        presets: ['es2016'],
       // plugins: ['react-html-attrs', 'transform-decorators-legacy', 'transform-class-properties'],
      }
    }
  ]
},
  plugins: debug ? [] : [
    new webpack.optimize.UglifyJsPlugin({ mangle: true, sourcemap: false }),
  ],
};