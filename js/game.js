
import PinBall from './objects/pinball'


let canvas = document.querySelector('#pin')

let game = new PinBall(canvas)

game.init();

//request animation frame stuff
let lastTime;
function callback(millis){
    if(lastTime) {
        game.update((millis-lastTime)/ 1000)
    }
    lastTime = millis

    requestAnimationFrame(callback);
}
callback();


//handle player 1 input
document.body.addEventListener('keydown', e => {
    const speed = 50
    switch(e.key){
        case 'ArrowUp':
            game.players[0].pos.y -= speed;
            break;
        case 'ArrowDown':
            game.players[0].pos.y += speed;
            break;
    }
})



canvas.addEventListener('mousemove', e => {
    //game.players[0].pos.x = e.offsetX;
    game.players[0].pos.y = e.offsetY - (game.players[0].height / 2);
    

    
})

canvas.addEventListener('click', _ => game.start())

