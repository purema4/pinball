export default class Vector {
    constructor(x=0,y=0) {
        this.x=x;
        this.y=y;
    }

    static get Vector0() {
        return new Vector(0,0)
    }

    getMirrorX(){
        return new Vector(this.x * -1, this.y)
    }

    getMirrorY() {
        return new Vector(this.x, this.y * -1)                
    }

    get lenght() {
        return Math.sqrt(this.x**2 + this.y**2)
    }

    set lenght(value){
        const fact = value / this.lenght;
        this.x *= fact
        this.y *= fact
    }

}