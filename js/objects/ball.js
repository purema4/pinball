import Rec from './rectangle';
import Vec from './vector';


export default class Ball extends Rec {
    constructor(vel=new Vec(0,0)) {
        super(new Vec(10,10))
        this.vel = vel
    }


    move(dt) {
        this.pos.x+= this.vel.x * dt;
        this.pos.y+= this.vel.y * dt;
    }
}