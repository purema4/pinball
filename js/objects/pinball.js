import Vector from "./vector";
import Rec from './rectangle';

export default class Pinball {
    constructor(canvas){
        this.canvas = canvas
        this.context = canvas.getContext('2d')
        this.background = new Rec(this.context, new Vector(this.canvas.width, this.canvas.height), Vector.Vector0, 'black')
        this.players = [
            new Rec(this.context, new Vector(10, 100), new Vector(10,this.background.middleY/2), 'white'),
            new Rec(this.context, new Vector(10, 100), new Vector(this.background.right - 20, this.background.middleY/2), 'white')
        ]
        this.ball = new Rec(this.context, new Vector(10,10))
        this.userMouse = new Rec(this.context, new Vector(10,10),Vector.Vector0, "pink")
    }

    init() {
        //random velocity
        this.ball.pos.x = this.background.middleX
        this.ball.pos.y = this.background.middleY

        this.ball.vel.x = 0;
        this.ball.vel.y = 0;
    }

    start() {
        //debugger
        if(this.ball.vel.x === 0 && this.ball.vel.y === 0) {
            this.ball.vel.y = Math.round(Math.random() * 400) - 100
            this.ball.vel.x = Math.round(Math.random() * 400) + 100
            this.ball.vel.x *= Math.random() > .5 ? 1 : -1 //random side
            this.ball.vel.lenght = 300
        }
    }

    win(playerId) {
        this.background.color = playerId === 0 ? 'red' : 'blue'
    }

    update(dt) {

        //logic
        this.ball.move(dt);
        if(this.ball.left <= 0 || this.ball.right >= this.background.size.x) {
            let playerId = +(this.ball.vel.x < 0)//checking win condition
            this.win(playerId)
            this.init()
        }
        if(this.ball.top <= 0 || this.ball.bottom >= this.background.size.y) {
            this.ball.vel = this.ball.vel.getMirrorY() //boundaries check\
        }

        this.players.forEach(p => {
            if(p.checkColisionWith(this.ball)) {
                //redirecting de ball depending where it collided
                this.ball.vel.y += (this.ball.bottom - p.middleY) * 2
                //reverse ball velocity
                this.ball.vel.x *= -1
                //renomarlise the speed
                this.ball.vel.lenght = 300
            }
        })  


        //Move our paddle
        //this.players[0].moveToY(this.userMouse, 200, dt)
        
       

        //AI
        this.players[1].moveToY(this.ball,200, dt)

        //paint
        //always updating background first
        this.background.draw();

       // this.userMouse.draw();

        this.ball.draw();

        this.players.forEach(p => {
            p.draw();
        })
    }
}