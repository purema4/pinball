import Vector from "./vector";

export default class Rectangle {
    
    constructor(context, size=Vector.Vector0, pos=Vector.Vector0, color="#fff", vel=Vector.Vector0) {
        this.pos = pos;
        this.size = size;
        this.color = color;
        this.vel = vel
        this.context = context
    }


    move(dt) {
        this.pos.x+= this.vel.x * dt;
        this.pos.y+= this.vel.y * dt;
    }

    moveToY(item, speed, dt){
        if(this.top-10 < item.top && this.bottom-10 > item.bottom){
            this.vel.y = 0
        }else if(this.bottom-10 < item.bottom){
            this.vel.y = speed
        }else{
            this.vel.y = -speed
        }

        this.move(dt)
    }


    checkColisionWith(item) {
        return this.left < item.right && this.right > item.left  && this.top < item.bottom && this.bottom > item.top  // check si l'item touch l'object
    }

    draw() {
        this.context.fillStyle=this.color;
        this.context.fillRect(this.pos.x, this.pos.y, this.size.x, this.size.y)
    }

    get left(){
        return this.pos.x
    }

    get right(){
        return this.pos.x + this.size.x
    }

    get top(){
        return this.pos.y
    }

    get height(){
        return this.bottom - this.top;
    }

     get width(){
        return this.right - this.left;
    }

    get bottom(){
        return this.pos.y + this.size.y
    }
    
    get middleX() {
        return this.pos.x + this.size.x / 2
    }

    get middleY(){
        return this.pos.y + this.size.y / 2
    }
}

